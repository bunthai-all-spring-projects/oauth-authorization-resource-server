package com.example.demo;

import com.example.demo.model.Account;
import com.example.demo.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.stream.Stream;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Autowired
    AccountRepository accountRepository;

    @Override
    public void run(String... args) throws Exception {
        Stream.of("thai,{noop}111", "bun,{noop}222", "deng,{noop}333")
                .map(accs -> accs.split(","))
                .forEach(acc -> accountRepository.save(new Account(acc[0], acc[1], true)));
    }

}
