package com.example.demo.controller.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class TestController {

    @RequestMapping("/hello")
    public String test() {
        return "Hello!";
    }

    @RequestMapping("/user")
    public Principal user(Principal principal) {
        return principal;
    }
}
